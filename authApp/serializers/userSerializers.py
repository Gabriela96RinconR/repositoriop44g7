from rest_framework import serializers
from authApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'Username', 'Nombre', 'Apellido', 'E-mail', 'Telefono celular', 'password']

    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance
    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)

        return {
                    'id': user.id,
                    'Username': user.username,
                    'Nombre': user.name,
                    'Apellido': user.last_name,
                    'E-mail': user.email,
                    'Telefono celular': user.cellphone,
                }
